# print-raw

Some tests on printing raw printer buffers directly from webpage to generic/text only printers

* basic.html
  * This method opens a new window, adds raw printer to window contents, and prints that window
* iframe.html
  * This method is similar to the first, except that it uses an iframe
  * This method requires a file that can be opened directly, for this example I use rawzpl.txt
* print-style.html
  * This method uses print-style sheets to show/hide content conditionally
  

Reference:
https://developer.mozilla.org/en-US/docs/Web/Guide/Printing

Samples should be active: https://jbread3.gitlab.io/print-raw/